# Sobki Bootstrap Starterkit


## Usage


### Before Drupal 10.3

You can copy/paste this theme in your `themes/custom` folder to init your own
sub-theme.

You will have to:
- change the machine names of files
- change the machine names inside those files
- remove the `hidden` key in the `.info.yml` file


### From Drupal 10.3

See the
[Starterkit documentation on Drupal.org](https://www.drupal.org/docs/core-modules-and-themes/core-themes/starterkit-theme).

Example command to generate your theme:

```bash
php core/scripts/drupal generate-theme my_theme --starterkit sobki_theme_bootstrap_starterkit --path themes/custom
```
