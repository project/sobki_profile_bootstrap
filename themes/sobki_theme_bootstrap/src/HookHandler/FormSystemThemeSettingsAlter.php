<?php

declare(strict_types=1);

namespace Drupal\sobki_theme_bootstrap\HookHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Alter theme settings form.
 */
class FormSystemThemeSettingsAlter {

  use StringTranslationTrait;

  /**
   * Alter theme settings form.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function alter(array &$form, FormStateInterface $formState): void {
    if (isset($form['logo']['settings']['logo_upload']['#description'])) {
      $form['logo']['settings']['logo_upload']['#description'] = \implode('<br>', [
        $form['logo']['settings']['logo_upload']['#description'],
        $this->t('An image with a height of 32 pixels is recommended for optimal display.'),
      ]);
    }
  }

}
