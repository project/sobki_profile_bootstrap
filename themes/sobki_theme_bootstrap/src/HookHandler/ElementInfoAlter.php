<?php

declare(strict_types=1);

namespace Drupal\sobki_theme_bootstrap\HookHandler;

use Drupal\sobki_theme_bootstrap\Element\ElementPreRenderMoreLink;

/**
 * Element Info Alter.
 */
class ElementInfoAlter {

  /**
   * Alter form element info.
   *
   * @param array $info
   *   An associative array with structure identical to that of the return value
   *   of \Drupal\Core\Render\ElementInfoManagerInterface::getInfo().
   */
  public function alter(array &$info): void {
    // More link.
    if (isset($info['more_link'])) {
      \array_unshift($info['more_link']['#pre_render'], [
        ElementPreRenderMoreLink::class,
        'preRenderMoreLink',
      ]);
    }
  }

}
