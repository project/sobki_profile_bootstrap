<?php

declare(strict_types=1);

namespace Drupal\sobki_theme_bootstrap\HookHandler;

use Drupal\ui_suite_bootstrap\Utility\Variables;

/**
 * Style views more links.
 */
class PreprocessMoreLink {

  /**
   * The Variables object.
   *
   * @var \Drupal\ui_suite_bootstrap\Utility\Variables
   */
  protected Variables $variables;

  /**
   * An element object provided in the variables array, may not be set.
   *
   * @var \Drupal\ui_suite_bootstrap\Utility\Element|false
   */
  protected $element;

  /**
   * Prepare more link container.
   *
   * @param array $variables
   *   The preprocessed variables.
   */
  public function preprocessContainer(array &$variables): void {
    $this->variables = Variables::create($variables);
    $this->element = $this->variables->element;
    if (!$this->element) {
      return;
    }

    if ($this->element->hasProperty('view')) {
      $this->variables->addClass([
        'row',
        'g-0',
        'my-3',
      ]);
    }
  }

}
