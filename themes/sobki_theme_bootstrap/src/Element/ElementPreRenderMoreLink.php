<?php

declare(strict_types=1);

namespace Drupal\sobki_theme_bootstrap\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Element prerender methods for more links.
 */
class ElementPreRenderMoreLink implements TrustedCallbackInterface {

  /**
   * Prerender a more link element.
   */
  public static function preRenderMoreLink(array $element): array {
    if (isset($element['#view'])) {
      $element += ['#options' => []];
      $element['#options'] = NestedArray::mergeDeepArray([
        $element['#options'],
        [
          'attributes' => [
            'class' => [
              'btn',
              'btn-primary',
            ],
          ],
        ],
      ]);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['preRenderMoreLink'];
  }

}
