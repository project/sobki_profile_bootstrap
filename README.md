# Sobki Profile Bootstrap

This profile is based on
[UI Suite Bootstrap](https://www.drupal.org/project/ui_suite_bootstrap) and its
goal is to provide a clean and ready to use Bootstrap 5 integration.

See [Sobki's documentation](https://project.pages.drupalcode.org/sobki)
for general and common documentation of the Sobki ecosystem.

There is currently no specific documentation for this profile.


## Requirements

This profile has no special system requirements. See
[Sobki's documentation](https://project.pages.drupalcode.org/sobki/installation/requirements)

This profile requires Composer to gather its dependencies. See
[Sobki's documentation](https://project.pages.drupalcode.org/sobki/installation/composer)


## Installation

Install as you would normally install a contributed Drupal profile.


## Configuration

After the installation, adapt the provided configuration to your needs.
