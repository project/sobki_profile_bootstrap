<?php

declare(strict_types=1);

namespace Drupal\sobki_assets\Service;

/**
 * A helper service to purge the destination directory.
 */
interface DestinationDirectoryPurgerInterface {

  public const ASSETS_CACHE_TAG = 'sobki_assets.assets';

  /**
   * Delete managed files inside AssetsUploadForm::DESTINATION_DIRECTORY.
   *
   * @param string|null $fid
   *   The file fid to purge. All files otherwise.
   */
  public function purge(?string $fid = NULL): void;

  /**
   * Delete AssetsUploadForm::DESTINATION_DIRECTORY if and only if it's empty.
   */
  public function removeEmptyDestinationDirectory(): void;

}
