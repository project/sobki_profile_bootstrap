<?php

declare(strict_types=1);

namespace Drupal\sobki_assets\Service;

/**
 * A helper service to extract the uploaded archive.
 */
interface ArchiveExtractorInterface {

  /**
   * Extract the archive to destination directory.
   *
   * @param string $archive_path
   *   Path to archive.
   * @param string $destination
   *   Directory of extraction.
   * @param callable $success
   *   Callback on success.
   * @param callable $fail
   *   Callback on failure.
   */
  public function extract(string $archive_path, string $destination, callable $success, callable $fail): void;

}
