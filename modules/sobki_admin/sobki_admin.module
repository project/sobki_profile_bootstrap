<?php

/**
 * @file
 * Contains hook implementations for the sobki_admin module.
 */

declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;
use Drupal\sobki_admin\HookHandler\FormAlter;
use Drupal\sobki_admin\HookHandler\FormLayoutBuilderBlockAlter;
use Drupal\sobki_admin\HookHandler\LibraryInfoAlter;
use Drupal\sobki_admin\HookHandler\PreprocessMenuToolbar;

/**
 * Implements hook_form_alter().
 */
function sobki_admin_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  /** @var \Drupal\sobki_admin\HookHandler\FormAlter $instance */
  $instance = \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(FormAlter::class);
  $instance->alter($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter() for 'layout_builder_add_block'.
 */
function sobki_admin_form_layout_builder_add_block_alter(array &$form, FormStateInterface $form_state): void {
  /** @var \Drupal\sobki_admin\HookHandler\FormLayoutBuilderBlockAlter $instance */
  $instance = \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(FormLayoutBuilderBlockAlter::class);
  $instance->formAlter($form, $form_state);
}

/**
 * Implements hook_form_FORM_ID_alter() for 'layout_builder_update_block'.
 */
function sobki_admin_form_layout_builder_update_block_alter(array &$form, FormStateInterface $form_state): void {
  /** @var \Drupal\sobki_admin\HookHandler\FormLayoutBuilderBlockAlter $instance */
  $instance = \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(FormLayoutBuilderBlockAlter::class);
  $instance->formAlter($form, $form_state);
}

/**
 * Implements hook_library_info_alter().
 */
function sobki_admin_library_info_alter(array &$libraries, string $extension): void {
  /** @var \Drupal\sobki_admin\HookHandler\LibraryInfoAlter $instance */
  $instance = \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(LibraryInfoAlter::class);
  $instance->alter($libraries, $extension);
}

/**
 * Implements hook_preprocess_HOOK() for 'menu__toolbar'.
 */
function sobki_admin_preprocess_menu__toolbar(array &$variables): void {
  /** @var \Drupal\sobki_admin\HookHandler\PreprocessMenuToolbar $instance */
  $instance = \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(PreprocessMenuToolbar::class);
  $instance->preprocess($variables);
}
