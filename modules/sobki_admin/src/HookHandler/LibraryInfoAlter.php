<?php

declare(strict_types=1);

namespace Drupal\sobki_admin\HookHandler;

/**
 * Alter libraries.
 */
class LibraryInfoAlter {

  /**
   * Alter libraries.
   *
   * @param array $libraries
   *   An associative array of libraries, passed by reference.
   * @param string $extension
   *   Can either be 'core' or the machine name of the extension that registered
   *   the libraries.
   */
  public function alter(array &$libraries, string $extension): void {
    if ($extension == 'navigation') {
      $libraries['navigation.layout']['dependencies'][] = 'sobki_admin/navigation.layout';
    }
  }

}
