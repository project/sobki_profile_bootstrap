<?php

declare(strict_types=1);

namespace Drupal\sobki_component_alert\Service;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Date utils service.
 */
interface DateUtilsServiceInterface {

  /**
   * Return seconds between date and current time.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   The date.
   *
   * @return int
   *   Return the number of seconds between the date and the current time.
   */
  public function getSecondsUntilDate(DrupalDateTime $date): int;

}
