<?php

declare(strict_types=1);

namespace Drupal\sobki_component_alert\Service;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Date utils service.
 */
class DateUtilsService implements DateUtilsServiceInterface {

  /**
   * Minute in seconds.
   */
  public const MINUTE_IN_SECONDS = 60;

  /**
   * Hour in minutes.
   */
  public const HOUR_IN_MINUTES = 60;

  /**
   * Hour in seconds.
   */
  public const HOUR_IN_SECONDS = self::HOUR_IN_MINUTES * self::MINUTE_IN_SECONDS;

  /**
   * Day in hours.
   */
  public const DAY_IN_HOURS = 24;

  /**
   * Day in seconds.
   */
  public const DAY_IN_SECONDS = self::DAY_IN_HOURS * self::HOUR_IN_SECONDS;

  /**
   * {@inheritdoc}
   */
  public function getSecondsUntilDate(DrupalDateTime $date): int {
    $now = new DrupalDateTime();
    $diff = $date->diff($now);

    // Convert $diff to seconds.
    $seconds = $diff->days * self::DAY_IN_SECONDS;
    $seconds += $diff->h * self::HOUR_IN_SECONDS;
    $seconds += $diff->i * self::MINUTE_IN_SECONDS;
    $seconds += $diff->s;

    return (int) $seconds;
  }

}
