<?php

declare(strict_types=1);

namespace Drupal\sobki_component_separator\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Provides a 'Separator' Block.
 */
#[Block(
  id: 'sobki_component_separator',
  admin_label: new TranslatableMarkup('Separator'),
  category: new TranslatableMarkup('Sobki')
)]
class Separator extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      'separator' => [
        '#type' => 'html_tag',
        '#tag' => 'hr',
      ],
    ];
  }

}
